create table history (
	anime_id int primary key, 
	anime text not null,
	genre text
);

insert into history values (
	0, 'anime-1', 'manga'
)

insert into history values 
(1, 'anime-1', 'manga'),
(2, 'anime-1', 'heroku')

insert into history values 
(1, 'anime-1', 'manga-6')

select * from history; 
select * from anime;
select * from genre;
select * from anime2genre;

drop rule push_to_storage on history
drop rule update_history on history
drop rule try_to_update on history

create rule update_history as on
update to history
do (
	insert into genre (genre) values
	(new.genre)
	on conflict(genre)
	do nothing;
	
	update anime2genre 
	set genre_id = (select genre_id from genre where genre = new.genre)
	where anime2genre.anime_id = new.anime_id 
)

create rule try_to_update as on
insert to history
where new.anime_id in (select anime_id from anime)
do INSTEAD update history
	set genre = new.genre
	where anime_id = new.anime_id

create rule push_to_storage as
on insert 
to history
where new.anime_id not in (select anime_id from anime)
do (insert into anime values (
		new.anime_id,
		new.anime);
	insert into genre (genre) values  (
		new.genre
	) on conflict(genre)
	do nothing;
	insert into anime2genre 
	(select new.anime_id,
	genre_id from genre
	where genre.genre = new.genre)
)

create table anime (
	anime_id int primary key, 
	anime text not null
);

create table genre (
	genre_id  serial primary key, 
	genre text not null unique
);

drop table genre CASCADE;

create table anime2genre (
	anime_id int not null,
	genre_id int not null
)

ALTER TABLE anime2genre
   ADD FOREIGN KEY (anime_id) REFERENCES anime;
   
ALTER TABLE anime2genre
   ADD FOREIGN KEY (genre_id) REFERENCES genre;
