drop table raw_data CASCADE;
create table raw_data (
	name varchar(255) primary key,
	ganre text[] DEFAULT '{}'::text[],
	episodes integer,
	duration integer,
	start_year integer,
	finish_year integer,
	description text,
	studious text[] DEFAULT '{}'::text[],
	tags text[] DEFAULT '{}'::text[],
	raiting float,
	votes integer
);

drop table anime CASCADE;
create table anime (
	name varchar(255) primary key,
	episodes integer,
	duration integer,
	start_year integer,
	finish_year integer,
	description text,
	raiting float,
	votes integer
);
	
drop table ganres CASCADE;
create table ganres (
	ganre_id serial primary key,
	ganre_name text not null unique
);

drop table anime2ganres CASCADE;
create table anime2ganres (
	name text references anime,
	ganre_id int references ganres
);

drop table tags CASCADE;
create table tags (
	tag_id serial primary key,
	tag_name text not null unique
);

drop table anime2tags CASCADE;
create table anime2tags (
	name text references anime,
	tag_id int references tags
);

drop table studious CASCADE;
create table studious (
	studio_id serial primary key,
	studio_name text not null unique
);

drop table anime2studious;

create table anime2studious (
	name text references anime,
	studio_id int references studious
);

insert into raw_data values
( 'anime-1', ARRAY['genre-3', 'genre-2']::text[], 6, 5, 20, 20, 'oh my', '{"studio-1"}',
'{"tag-1", "tag-4"}', 6.5, 5);


select * from raw_data;
select * from anime;
select * from ganres;
select * from studious;
select * from tags;
select * from anime2tags;

CREATE TRIGGER before_all after insert 
    ON raw_data
    FOR EACH ROW EXECUTE PROCEDURE parse();

create TRIGGER update_raw_before before UPDATE
 on raw_data
 for each row execute procedure update();


create or replace function update()
returns trigger
as
$$
DECLARE
    r text;
begin
		update anime set
		episodes = NEW.episodes,
		duration = NEW.duration,
		start_year = NEW.start_year,
		finish_year = NEW.finish_year,
		description = NEW.description,
		raiting = NEW.raiting,
		votes = NEW.votes
		where name = NEW.name;

		
		FOR r IN SELECT x.* from unnest(NEW.ganre::text[]) as x
		LOOP
		    insert into ganres (ganre_name) values
		    (r)
		    on conflict(ganre_name)
			do nothing;

		    update anime2ganres
			set
			name = NEW.name
			where name = OLD.name and ganre_id in (select ganre_id from ganres
			where ganres.ganre_name = r);
		END LOOP;


		FOR r IN SELECT x.* from unnest(NEW.tags::text[]) as x
		LOOP
		   insert into tags (tag_name) values
		   (r)		
			on conflict(tag_name)
			do nothing; 

			update anime2tags
			set 
			name = NEW.name
			where name = OLD.name and tag_id in (select tag_id from tags
			where tags.tag_name = r);
		END LOOP;

		FOR r IN SELECT x.* from unnest(NEW.studious::text[]) as x
		LOOP
		   insert into studious (studio_name) values
		   (r)		
			on conflict(studio_name)
			do nothing;

			update anime2studious
			set 
			name = NEW.name
			where name = OLD.name and studio_id in (select studio_id from studious
			where studious.studio_name = r);

		END LOOP;
		return NEW;
	
end;
$$
language plpgsql;



create or replace function parse()
returns trigger
as
$$
DECLARE
    r text;
begin
    if (NEW.name not in (select name from anime)) 
		then 
		insert into anime values (
			new.name,
			new.episodes,
			new.duration,
			new.start_year,
			new.finish_year,
			new.description,
			new.raiting,
			new.votes);

		FOR r IN SELECT x.* from unnest(NEW.ganre::text[]) as x
		LOOP
		   insert into ganres (ganre_name) values
		   (r)		
			on conflict(ganre_name)
			do nothing; 

			insert into anime2ganres
			(select new.name,
			ganre_id from ganres
			where ganres.ganre_name = r);
		END LOOP;


		FOR r IN SELECT x.* from unnest(NEW.tags::text[]) as x
		LOOP
		   insert into tags (tag_name) values
		   (r)		
			on conflict(tag_name)
			do nothing; 

			insert into anime2tags
			(select new.name,
			tag_id from tags
			where tags.tag_name = r);
		END LOOP;

		FOR r IN SELECT x.* from unnest(NEW.studious::text[]) as x
		LOOP
		   insert into studious (studio_name) values
		   (r)		
			on conflict(studio_name)
			do nothing;

			insert into anime2studious
			(select new.name,
			studio_id from studious
			where studious.studio_name = r);

		END LOOP;

		return NEW;
	end if;
	return NULL;
	
end;
$$
language plpgsql;

