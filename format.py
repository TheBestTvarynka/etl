import ast
import math
import pandas
import psycopg2
import numpy as np

def generateId(name):
    # here should be cool chaining but Python sucks
    return ''.join(list(filter(lambda ch: ch != ' ', list(name.casefold()))))

animeData = pandas.read_csv('anime.csv')
animeDataDict = {}
for row in animeData.values:
    animeDataDict[row[1]] = row
print(len(animeDataDict))

animeData2 = pandas.read_csv('anime2.csv')
animeDataDict2 = {}
for row in animeData2.values:
    animeDataDict2[row[0]] = row
print(len(animeDataDict2))

same = {}
animeDataDict1 = {}
for key in animeDataDict:
    value2 = animeDataDict2.get(key)
    if value2 is not None:
        same[key] = np.concatenate((animeDataDict[key], value2), axis=0)
        del animeDataDict2[key]
    else:
        animeDataDict1[key] = animeDataDict[key]

print('------')
print(len(animeDataDict1))
print(len(animeDataDict2))
print(len(same))
print('------')

connection = psycopg2.connect("host=localhost dbname=etl user=postgres password=postgres")
# connection = psycopg2.connect("host=localhost dbname=test user=postgres password=postgres")
# неа. працює вставлення в tags. але загальне ні. зараз покажу
cur = connection.cursor()
for name in same:
    values = same[name]
    if values[4] == 'Unknown':
        values[4] = -1
    else:
        values[4] = int(values[4])
    cur.execute('insert into raw_data values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
            (values[1],
                [] if not isinstance(values[2], str) and math.isnan(values[2]) else values[2].split(' '),
                values[4],
                -1 if math.isnan(values[10]) else values[10],
                -1 if math.isnan(values[12]) else int(values[12]),
                -1 if math.isnan(values[13]) else int(values[13]),
                values[15],
                ast.literal_eval(values[16]),
                ast.literal_eval(values[17]),
                -1 if math.isnan(values[23]) else values[23],
                -1 if math.isnan(values[24]) else values[24]))
connection.commit()
